<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Noticias;
use yii\data\ActiveDataProvider;
use app\models\Formulario;
use app\models\Comentarios;
use app\models\NoticiasFotos;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Fotos;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Noticias::find()]);
        return $this->render("index",["dataProvider" => $dataProvider]);
         
    }
    
    public function actionAnadircomentario($codigo)
    {
        $model = new Comentarios();

        if ($model->load(Yii::$app->request->post())) {
            $model->noticias_codigo=$codigo;
            if ($model->save()) 
            {
                return $this->redirect(["site/vercomentarios","codigo"=>$codigo]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
        ]);
    
    }
    
    public function actionVercomentarios($codigo)
    {
        $noticia= Noticias::findOne($codigo);
        $dataProvider = new ActiveDataProvider([
        'query' => Comentarios::find()->where(['noticias_codigo' => $codigo])]);
        return $this->render("listar",["dataProvider" => $dataProvider,"noticia" => $noticia]);
    }
    
    public function actionEditarcomentario($codigo)
    {
        $model=Comentarios::findOne($codigo);
        
        if($this->request->isPost)
        {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) 
                {
                    return $this->redirect(["site/vercomentarios","codigo"=>$model->noticias_codigo]);
                }
            }
        }
        
        return $this->render('formulario', [
            'model' => $model,
        ]);
                
    }
    
    public function actionEliminarcomentario($codigo)
    {
        $model=Comentarios::findOne($codigo);
        $noticias_codigo=$model->noticias_codigo;
        $model->delete();
        return $this->redirect(["site/vercomentarios","codigo"=>$noticias_codigo]);
    }
    
    public function actionNuevanoticia()
    {
        $model=new Noticias();
        $model_2=new NoticiasFotos();
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(["site/index"]);
                }
        }

        return $this->render('formulario_noticias', [
            'model' => $model,
        ]);
    }
    
    public function actionEditarnoticia($codigo)
    {
        $model= Noticias::findOne($codigo);
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(["site/index"]);
                }
        }

        return $this->render('formulario_noticias', [
            'model' => $model,
        ]);
    }
    
    public function actionEliminarnoticia($codigo)
    {
        $model=Noticias::findOne($codigo);
        $model->delete();
        return $this->redirect(["site/index"]);
    }
    
    public function actionConfirmareliminarnoticia($codigo)
    {
        $model=Noticias::findOne($codigo);
        
        return $this->render("ver",['model'=> $model]);
    }

    public function actionAnadirfoto()
    {
        $model = new Fotos();

        if ($model->load(Yii::$app->request->post())) {
            echo "<br><br><br><br><br>";
            var_dump($model);
            $model->nombre=UploadedFile::getInstance($model,"nombre");
 
            if ($model->save()) {
                 return $this->redirect(["site/index"]);
            }
            
            
        }

        return $this->render('formulario_fotos', [
            'model' => $model,
        ]);
    }
    
    public function actionEditarfoto($codigo)
    {
        $model = Fotos::findOne($codigo);
        if($this->request->post())
        {
            if ($model->load(Yii::$app->request->post())) 
            {
                $model->nombre=UploadedFile::getInstance($model,"nombre");

                if ($model->save()) {
                     return $this->redirect(["site/verfotos"]);
                }
            }
        }
    }
    
    public function actionEliminarfoto($codigo)
    {
        $model= Fotos::findOne($codigo);
        $model->delete();
        return $this->redirect(["site/verfotos"]);
        
    }
    
    public function actionVerfotos()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Fotos::find()]);
        return $this->render("listar_fotos",["dataProvider" => $dataProvider]);
    }
    
    public function actionNoticiasfotos()
    {
        $model=new NoticiasFotos();
        
        if ($model->load(Yii::$app->request->post())) 
        {
            $correcto=[];
            foreach ($model->fotos_codigo as $foto)
            {
                $modelo=new NoticiasFotos();
                $modelo->noticias_codigo=$model->noticias_codigo;
                $modelo->fotos_codigo=$foto;
                if($modelo->save())
                {
                    $correcto[]=Fotos::findOne($foto)->nombre;
                }
            }
            
            if (count($correcto)>0) {
                return $this->render('salidanoticiafotos',[
                    'correctas'=>$correcto,
                ]);
            }else{
                $model->addError("cod_foto","todas las fotos estan ya en la noticia");
            }

        }
        $noticias= Noticias::find()->all();
        $listar_noticias= ArrayHelper::map($noticias,'codigo',function($model)
        {
            return $model->codigo . "-" . $model->titulo; 
        }
        );
        

        $fotos= Fotos::find()->all();
        
        $listar_fotos= ArrayHelper::map($fotos,'codigo',function($model){
                return Html::img("@web/imgs/" . $model->nombre,["class"=>'col-lg-10 img-fluid']);
            });

        return $this->render('noticias_fotos', [
            'model' => $model,
            'listar_noticias' => $listar_noticias,
            'listar_fotos' => $listar_fotos,
        ]);
    }
    
    

    
}
