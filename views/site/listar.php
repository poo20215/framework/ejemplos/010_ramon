<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap4\Carousel;

?>
<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5"><?= $noticia->titulo ?></h1>
<h4><?= $noticia->texto ?></h4>
<br>
Comentarios de la noticia
<?php
$carusel=[];
$fotos=$noticia->getFotosCodigos()->all();//obteniendo datos de otra tabla con la que está relaccionada
foreach($fotos as $foto)
{
    $carusel[]=Html::img("@web/imgs/$foto->nombre",["width"=>300,"height"=>200]);
}

echo Carousel::widget([
    'items' => $carusel,
    'options'=>[
        'class' => 'mx-auto col-lg-8'
    ],
    'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']
]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' =>"{items}",
    'columns' =>[
        "texto",
        "fecha",
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{editar} {eliminar}',
            'buttons' => [
                'editar' => function ($url,$model) {
                    return Html::a('<i class="fas fa-pencil-alt"></i>', 
                       ['site/editarcomentario',"codigo"=>$model->codigo]);
                },
                'eliminar' => function ($url,$model) {
                    return Html::a('<i class="far fa-trash-alt"></i>',
                            ['site/eliminarcomentario',"codigo"=>$model->codigo],
                            ['data' => ['confirm' => '¿Estas seguro que deseas borrar el comentario?',
                                        'method' => 'post']]);
                },
                        ]
        ]
                    ]         
]);
                
?>                


