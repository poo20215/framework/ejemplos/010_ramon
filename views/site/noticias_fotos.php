<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NoticiasFotos */
/* @var $form ActiveForm */
?>
<div class="site-noticias_fotos">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'noticias_codigo')->dropDownList($listar_noticias) ?>
        <?= $form->field($model, 'fotos_codigo',['options'=>['class'=>'col-lg-3 p-0']])->checkboxList(
                $listar_fotos,
                ['prompt'=>'Selecciona una imagen','encode'=>false]
            ); ?>
        <?= $form->field($model, 'visitas') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-noticias_fotos -->
