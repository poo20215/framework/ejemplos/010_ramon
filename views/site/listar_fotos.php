<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' =>"{items}",
    'columns' =>[
        "codigo",
        [
            'label'=>'Foto',
            'format'=>'raw',
            'value'=>function($foto){
                $url='@web/imgs/' . $foto->nombre;
                return Html::img($url,['class'=>'img-fluid', 'style'=>'width:300px']);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{editar} {eliminar}',
            'buttons' => [
                'editar' => function ($url,$model) {
                    return Html::a('<i class="fas fa-pencil-alt"></i>', 
                       ['site/editarfoto',"codigo"=>$model->codigo]);
                },
                'eliminar' => function ($url,$model) {
                    return Html::a('<i class="far fa-trash-alt"></i>',
                            ['site/eliminarfoto',"codigo"=>$model->codigo],
                            ['data' => ['confirm' => '¿Estas seguro que deseas eliminar la foto?',
                                        'method' => 'post']]);
                },
                        ]
        ]
                    ]         
]);
                
?>
