<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Noticias[] $noticiasCodigos
 * @property NoticiasFotos[] $noticiasFotos
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'],'required'],
            [['nombre'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Foto',
        ];
    }

    /**
     * Gets query for [[NoticiasCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasCodigos()
    {
        return $this->hasMany(Noticias::className(), ['codigo' => 'noticias_codigo'])->viaTable('noticias_fotos', ['fotos_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['fotos_codigo' => 'codigo']);
    }
    
    public function beforeSave($insert) 
    {
        parent::beforeSave($insert);
        
        //vamos a gestionar la subida de fotos
        if(!isset($this->nombre))
        {
           //en caso de que no indiquemos foto nos elige la antigua
            //$this->nombre=$this->getOldAttribute("nombre");
        }
        return true;
    }
    
    public function afterSave($insert, $changedAttributes) 
    {
        parent::afterSave($insert, $changedAttributes);
        // la funcion iconv la utilizo para que saveAs no me de problemas con las tildes y ñ
        $this->nombre->saveAs('imgs/id_' . $this->codigo . '_' . iconv('UTF-8','ISO-8859-1',$this->nombre->name), false);
        $this->nombre = $this->codigo . '_' . iconv('UTF-8','ISO-8859-1', $this->nombre->name);
        
        $this->updateAttributes(["nombre"]);

    }
}
