<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "noticias".
 *
 * @property int $codigo
 * @property string|null $titulo
 * @property string|null $texto
 * @property string|null $fecha
 *
 * @property Comentarios[] $comentarios
 * @property Fotos[] $fotosCodigos
 * @property NoticiasFotos[] $noticiasFotos
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['noticias_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[FotosCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFotosCodigos()
    {
        return $this->hasMany(Fotos::className(), ['codigo' => 'fotos_codigo'])->viaTable('noticias_fotos', ['noticias_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['noticias_codigo' => 'codigo']);
    }
    /*
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
    }
    */
    //Este metodo se ejecuta automaticamente antes de insertar o modificar una noticia
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");
        return true;

    }
    
    //Este metodo se ejecuta despues de realizar un find ==> select * from noticias
    public function afterFind()
    {
        //voy a colocar la fecha en formato dia/mes/año cuando muestre registros
        parent::afterFind();
        if(!empty($this->fecha))
        {
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');
        }

    }
}
