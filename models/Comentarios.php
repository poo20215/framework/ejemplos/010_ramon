<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $codigo
 * @property string|null $texto
 * @property string|null $fecha
 * @property int|null $noticias_codigo
 *
 * @property Noticias $noticiasCodigo
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string'],
            [['fecha'], 'safe'],
            [['noticias_codigo'], 'integer'],
            [['noticias_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['noticias_codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'noticias_codigo' => 'Noticias Codigo',
        ];
    }

    /**
     * Gets query for [[NoticiasCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasCodigo()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'noticias_codigo']);
    }
    
    //Este metodo se ejecuta automaticamente antes de insertar o modificar un comentario
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha=date('Y-m-d');
        return true;

    }
    
    //Este metodo se ejecuta despues de realizar un find ==> select * from comentarios
    public function afterFind()
    {
        //voy a colocar la fecha en formato dia/mes/año cuando muestre registros
        parent::afterFind();
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');

    }
}
